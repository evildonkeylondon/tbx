import React from 'react';
import { connect, styled } from "frontity";
import Logo from "../header/logo";
import Link from '../ui/link';

const Footer = ({ state, libraries }) => {
    const options = state.source.get("theme-tbx-settings");
    const { copyright_text } = options.acf;
    const items = state.source.get(`/menu/${state.theme.footerMenu}/`).items;
    return (
        <FooterWrapper>
            <div className="container">
                <div className="row flex-column flex-md-row justify-content-between">
                    <div className="col-md-auto mb-4 mb-md-0">
                        <Logo />
                    </div>
                    <div className="col-md-auto order-last order-md-2">
                        <small>{copyright_text}</small>
                    </div>
                    <div className="col-md-auto mb-4 mb-md-0 order-first order-md-3">
                        <ul className="d-flex flex-wrap flex-column flex-md-row list-unstyled m-0 p-0">
                            {items && items.map((item) => {
                                const name = item.title;
                                const link = libraries.source.normalize(item.url);
                                const isCurrentPage = state.router.link === link;
                                const Html2React = libraries.html2react.Component;
                                
                                return (
                                    <li key={item.ID} className="me-md-4">
                                        <Link 
                                            link={link} 
                                            aria-current={isCurrentPage ? "page" : null}
                                            children={<Html2React html={name} />}
                                            className="footer--menu-nav"
                                        />
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        </FooterWrapper>
    );
}

export default connect(Footer);

const FooterWrapper = styled.div`
    width: 100%;
    display: inline-block;
    padding: 40px 0;
    background-color: var(--colour-blue-00);
    @media screen and (min-width: 992px) {
        padding: 180px 40px 20px;
    }
    small {
        color: var(--colour-white-00);
        font-size: 12px;
        line-height: 14px;
    }

    .footer--menu-nav {
        color: var(--colour-white-00);
        text-decoration: none;
        transition: var(--transition-normal);
        font-size: var(--font-size-nav);
        font-family: var(--font-bold);
        &:hover {
            color: var(--colour-blue-01);
        }
    }
`;