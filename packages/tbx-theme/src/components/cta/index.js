import React from 'react';
import { connect, styled } from 'frontity';
import Button from '../ui/button';

const Cta = ({ cta, className }) => {

    return (
        <div className={`d-flex flex-wrap ${className ? className : ''}`}>
            {cta.map((btn, i) => {
                return (
                    <div key={i.toString()} className="me-2">
                        <Button
                            link={btn.cta_link} 
                            text={btn.cta_text}
                            colour={btn.cta_colour}
                            external={btn.cta_external_url}
                        />
                    </div>
                );    
            })}
        </div>
    );
};

export default connect(Cta);