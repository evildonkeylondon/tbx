import React from 'react';
import { connect, styled } from 'frontity';

const Dropdown = ({ items, title, state, actions }) => {

    var str2bool = (value) => {
        if (value && typeof value === "string") {
             if (value.toLowerCase() === "true") return true;
             if (value.toLowerCase() === "false") return false;
        }
        return value;
     }

    const dropdownChange = (e) => {
        const id = e.target.value;
        const type = e.target.dataset.name;

        actions.theme.dropdownChange(str2bool(id));
        actions.theme.dropdownChangeType(type);

        actions.theme.updateStoredGridItems(str2bool(id), type, true);
    }

    return (
        <select className="me-md-4 mb-4 mb-md-0 news--dropdown" onChange={e => dropdownChange(e)} data-name={title}>
            <option value={false}>{title.charAt(0).toUpperCase() + title.slice(1)}</option>
            {items && items.map((item, i) => {
                const post = state.source[item.type][item.id];
                return (
                    <option key={post.id} value={post.id} type={post.type}>{post.title.rendered}</option>
                )
            })}
        </select>
    );
};

export default connect(Dropdown);