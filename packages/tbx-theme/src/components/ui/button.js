import React from 'react';
import { connect, styled } from 'frontity';
import Link from './link';

const Button = ({ colour, link, text, external }) => {
    return (
        <ButtonWrapper colour={colour}>
            <Link link={external ? external : link} target={external && `_blank`}>
                <span>{text}</span>
                <svg width="9" height="17" viewBox="0 0 9 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.99991 16L6.33398 8.49989L1.99991 1" stroke="#00C7EE" strokeWidth="3" strokeMiterlimit="10"/></svg>
            </Link>
        </ButtonWrapper>
    );
};

export default connect(Button);

const ButtonWrapper = styled.div`
    margin: 0;
    a {
        font-family: var(--font-bold);
        padding: 12px;
        color: inherit;
        color: ${props => (props.colour == "light") ? 'var(--colour-blue-00)' : 'var(--colour-blue-01)'};
        border: 2px solid var(--colour-blue-01);
        font-size: 16px;
        line-height: 16px;
        text-transform: uppercase;
        display: flex;
        align-items: center;
        text-decoration: none !important;
        transition: all .3s ease;
        word-spacing: .05em;
        scroll-behaviour: smooth;
        &:hover {
            color: var(--colour-blue-00);
            background-color: var(--colour-blue-01);
            svg {
                transform: translateX(2px);
                path {
                    stroke: var(--colour-blue-00);
                }
            }
        }
        svg {
            margin-left: 20px;
            transition: var(--transition-normal);
            path {
                transition: var(--transition-normal);
            }
        }
    }
`;