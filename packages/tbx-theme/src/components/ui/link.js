import React, { useEffect } from "react";
import { connect, styled } from "frontity";

const Link = ({ 
    state, 
    actions, 
    link, 
    className, 
    id,
    colour, 
    children, 
    "aria-current" : ariaCurrent, 
    target, 
}) => {

    const isExternal = link.startsWith("http");

    useEffect(() => {
        if (!isExternal) {
            if (state.theme.autoPrefetch === "all") actions.source.fetch(link);
        }
    }, []);

    const onClick = (event) => {
        if (isExternal) return;
        event.preventDefault();
        actions.theme.closeMobileMenu();
        state.theme.postsToShow = [];
        actions.router.set(link);
        window.scroll(0, 0);
        document.querySelector('body').scrollTo(0,0);
    };

    return (
        <LinkItem
            href={link}
            onClick={onClick}
            className={className || null}
            id={id || null}
            aria-current={ariaCurrent}
            target={target || null}
            colour={colour}
            onMouseEnter={() => {
                // Prefetch the link's content when the user hovers on the link
                if (state.theme.autoPrefetch === "hover" && !isExternal)
                  actions.source.fetch(link);
            }}
        >
            {children}
        </LinkItem>
    );
};

export default connect(Link);

const LinkItem = styled.a`
    transition: all .3s ease-out;
    color: ${props => props.colour};
`;