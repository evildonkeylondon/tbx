import React from 'react';
import { connect, styled } from 'frontity';

const Column = ({ state, content, libraries, sideText }) => {
    const Html2React = libraries.html2react.Component;
    return(
        <ColumnWrapper sideText={sideText}>
            <Html2React html={content} />
        </ColumnWrapper>
    );
};

export default connect(Column);

const ColumnWrapper = styled.div`
    p, li {
        font-size: ${props => props.sideText ? '18px' : 'inherit'};
        line-height: ${props => props.sideText ? '24px' : 'inherit'};
        a {
            color: inherit;
            text-decoration: none;
            transition: color .3s ease;
            scroll-behaviour: smooth;
            &:hover {
                color: var(--color-blue-01);
            }
        }
        img {
            height: auto !important;
            width: 100%;
        }
    }
`;