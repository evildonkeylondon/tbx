import React from "react";
import { connect, Global, styled } from "frontity";
import Switch from "@frontity/components/switch";
import globalStyle from './globalStyle';
import bootstrap from '../assets/css/bootstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { useTransition, animated } from "react-spring";
import Header from './header';
import Footer from './footer';
import Loading from './ui/loading';
import PostType from './layout/postType';
import Homepage from './layout/homepage';
import FlexiblePage from './layout/flexiblePage';
import News from './layout/news';
import Events from './layout/events';
import Post from './layout/post';
import Banner from './banner';
import Contact from './layout/contact';
import Default from './layout/default';

library.add(fab);

const Root = ({ state }) => {
  const data = state.source.get(state.router.link);

  let showBanner = false;
  let flexiblePage = false;
  let contact = false;
  let blog = false;
  let events = false;

  if (data.isServices || data.isSolutions || data.isIndustries || data.isArchive || data.isEvents) {
    showBanner = true;
  }

  if (data.link == "/our-approach/" || data.link == "/about-us/" || data.link == "/partners/") {
    flexiblePage = true;
    showBanner = true;
  }

  if (data.link == "/contact-us/") {
    contact = true;
  }

  if (data.link == "/our-insights/") {
    blog = true;
  }

  if (data.link == "/our-events/") {
    events = true;
  }

  const transitions = useTransition(state.router.link, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
    config: { duration: 300 }
  }); 

  return (
    <>
      <Global styles={bootstrap} />
      <Global styles={globalStyle}/>
      <Header data={data} />
      {transitions((style, route) => {
        return (
          <animated.div style={style}>
            <Absolute>
              <Main>
                <Switch>
                  <Loading when={data.isFetching} />
                  <PostType when={data.isSolutions || data.isServices || data.isIndustries} data={data} />
                  <Homepage when={data.isHome} data={data} />
                  <FlexiblePage when={flexiblePage} data={data} />
                  <Contact when={contact} data={data} />
                  <Events when={events} data={data} />
                  <News when={blog} data={data} />
                  <Post when={data.isPost} data={data} />
                  <Default data={data} />
                </Switch>
              </Main>
              {showBanner && <Banner />}
              <Footer data={data}/>
            </Absolute>
          </animated.div>
        )
      })}
    </>
  )
};

export default connect(Root);

const Main = styled.main`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column; 
  flex-grow: 1; 
  width: 100%;
  overflow: hidden;
`;

const Absolute = styled.div`
    position: absolute;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    flex-grow: 1;
    min-height: 100vh;
`;