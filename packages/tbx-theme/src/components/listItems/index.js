import React, { useEffect } from 'react';
import { connect, styled } from "frontity";
import Link from '../ui/link';

const ListItems = ({ state, actions, items }) => {

    const solutions = state.source.get('/solutions/');
    const services = state.source.get('/services/');
    const industries = state.source.get('/industries/');

    useEffect(() => {
        actions.source.fetch('/solutions/');
        actions.source.fetch('/services/');
        actions.source.fetch('/industries/');
    }, []);

    return solutions.isReady && services.isReady && industries.isReady ? (
        <ListWrapper>
            {items && items.map((item, i) => {
                const { post_title, post_type, ID } = item;
                const serviceData = state.source[post_type][ID];
                const { link } = serviceData;
                const { icon } = serviceData.acf;
                return (
                    <Link key={i.toString()} link={link} className="d-flex align-items-center mb-3">
                        {icon && <figure className="d-flex me-2"><img src={icon} alt={post_title} /></figure>}
                        <span className="me-2">{post_title}</span>
                        <svg width="9" height="17" viewBox="0 0 9 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.99991 16L6.33398 8.49989L1.99991 1" stroke="#00C7EE" strokeWidth="3" strokeMiterlimit="10"/></svg>
                    </Link>
                )
            })}
        </ListWrapper>
    ) : null;
}

export default connect(ListItems);

const ListWrapper = styled.div`
    a {
        text-decoration: none;
        color: var(--colour-blue-00);
    }
    span {
        font-family: var(--font-bold);
        font-size: 16px;
        line-height: 16px;
        text-transform: uppercase;
        transition: all .3s ease;
        letter-spacing: 0.05em;
    }
    figure {
        max-width: 30px;
    }
`;