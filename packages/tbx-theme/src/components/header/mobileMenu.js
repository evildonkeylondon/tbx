import { styled, connect, Global } from "frontity";
import { CloseIcon, HamburgerIcon } from "./menuIcon";
import MenuModal from "./menuModal";

function MobileMenu({ state, actions, hamburgerColour }) {
  const { isMobileMenuOpen } = state.theme;
  return (
    <>
      <MenuToggle onClick={actions.theme.toggleMobileMenu}>
        {isMobileMenuOpen ? (
          <>
            <Global styles={{ body: { overflowY: "hidden" } }} />
            <CloseIcon color="white" size="22px" />
          </>
        ) : (
          <HamburgerIcon color={hamburgerColour} size="30px" />
        )}
      </MenuToggle>
      <MenuModal />
    </>
  );
}

export default connect(MobileMenu);

const MenuToggle = styled.button`
  position: absolute;
  right: 10px;
  top: 15px;
  background: transparent;
  border: 0;
  color: white;
  z-index: 5;
  height: 40px;
  width: 40px;
  display: none;

  @media (max-width: 991px) {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;