import React from 'react';
import { connect, styled } from "frontity";
import Logo from "./logo";
import Nav from './nav';
import MobileMenu from "./mobileMenu";

const Header = ({ state }) => {
    const { white } = state.theme.colours;
    return (
        <HeaderWrapper className="container-fluid p-0 position-fixed top-0 start-0 w-100">
            <div className="row">
                <div className="col d-flex justify-content-between align-items-center">
                    <div className="ms-3"><Logo /></div>
                    <Nav />
                    <MobileMenu hamburgerColour={white} />
                </div>
            </div>
        </HeaderWrapper>
    )
};

export default connect(Header);

const HeaderWrapper = styled.div`
    background-color: var(--colour-blue-00);
    z-index: 1000;
    padding-top: 15px !important;
    padding-bottom: 15px !important;
    @media (min-width: 992px) {
        padding: 0 !important;
    }
`;