import React, { useState } from 'react';
import { connect, styled } from "frontity";
import Link from "../ui/link";

const Logo = ({ state }) => {
    const [ loaded, setLoaded ] = useState(false);
    const options = state.source.get("theme-tbx-settings");
    const { logo } = options.acf;
    return (
        <Figure className="mb-0">
            <Link link="/">
                <img
                    src={logo} 
                    className={loaded ? "loaded" : null}
                    onLoad={() => setLoaded(true)}
                    loading="lazy" 
                    alt="TBX Logo"
                />
            </Link>
        </Figure>
    );
};

export default connect(Logo);

const Figure = styled.figure`
    width: 100%;
    max-width: 88px;
    a {
        width: 100%;
    }
    img {
        vertical-align: bottom;
        width: 100%;
        opacity: 0;
        visibility: hidden;
        transition: opacity .3s ease;
        &.loaded {
            opacity: 1;
            visibility: visible;
        }
    }
`;