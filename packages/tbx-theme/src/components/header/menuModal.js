import { styled, connect } from "frontity";
import Logo from './logo';
import Link from '../ui/link';

const MenuModal = ({ state, libraries }) => {
    const { white, blue_0 } = state.theme.colours;
    const { isMobileMenuOpen } = state.theme;
    const items = state.source.get(`/menu/${state.theme.mainMenu}/`).items;

    return (
        <>
            <MenuOverlay bg={blue_0} className={isMobileMenuOpen ? "show" : null}>
                <LogoModal>
                    <div className="ms-3"><Logo /></div>
                </LogoModal>
                <MenuContent as="nav">
                    {items.map((item) => {
                        const name = item.title;
                        const subItems = item.child_items;
                        const link = libraries.source.normalize(item.url);
                        const isCurrentPage = state.router.link === link;
                        
                        return (
                            <NavItem key={item.ID} isSelected={isCurrentPage} colour={white}>
                                <Link 
                                    link={link} 
                                    aria-current={isCurrentPage ? "page" : null} 
                                    className="menu--nav-item" 
                                    children={name} 
                                    colour={white}
                                />
                                {subItems && <ul className="position-absolute">
                                    {subItems.map((item) => {
                                        const name = item.title;
                                        const link = libraries.source.normalize(item.url);
                                        return (
                                            <li key={item.ID}>
                                                <Link link={link} children={name} />
                                            </li>
                                        )
                                    })}
                                </ul>}
                            </NavItem>
                        )
                    })}
                </MenuContent>
            </MenuOverlay>
        </>
    );
};

const MenuOverlay = styled.div`
    background-color: ${props => props.bg};
    width: 100vw;
    height: 100vh;
    overflow: hidden auto;
    position: fixed;
    z-index: 2;
    top: 0;
    left: 0;
    transform: translateX(100%);
    display: flex;
    justify-content: center;
    align-items: center;
    &.show {
        transform: translateX(0);
    }
`;

const LogoModal = styled.div`
    position: absolute;
    top: 15px;
    left: 0;
`;

const MenuContent = styled.ul`
    z-index: 3;
    display: flex;
    flex-direction: column;
    margin: 0;
    padding: 0 15px;
    list-style: none;
    justify-content: center;
    height: 100%;
    width: 100%;
`;

const NavItem = styled.li`
    position: relative;
    align-self: flex-start;
    a {
        text-decoration: none;
        font-family: var(--font-bold);
        text-transform: uppercase;
        word-spacing: .04em;
        font-size: 25px;
        padding: 0;
        z-index: 1;
        display: inline-block;
        line-height: 40px;
        white-space: nowrap;
        word-spacing: .04em;
        color: var(--colour-white-00);
        svg {
            transform: translateY(-5px);
            path {
                fill: var(--colour-white-00);
            }
        }
    }
`;

const HeaderArrow = styled.div`
    position: absolute;
    border-style: solid;
    border-color: var(--white-0);
    width: 20px;
    height: 20px;
    border-width: 1px 1px 0 0;
    transform: rotate(45deg);
    top: 28px;
    right: -25px;
    z-index: 0;
    transition: left 140ms cubic-bezier(0,0,.2,1),right 140ms cubic-bezier(0,0,.2,1);
`;

export default connect(MenuModal);
