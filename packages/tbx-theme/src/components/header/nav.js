import React from 'react';
import { connect, styled } from "frontity";
import NavItem from './navItem';
import Link from '../ui/link';

const Nav = ({ state, colour }) => {
    const items = state.source.get(`/menu/${state.theme.mainMenu}/`).items;
    return (
        <div className="d-flex align-items-center">
            <NavWrapper>
                <NavItem items={items} />
            </NavWrapper>
            <NavButton className="nav--item_custom-1"><Link link="#" children="Login" /></NavButton>
            <NavButton className="nav--item_custom-2"><Link link="#" children="Register" /></NavButton>
        </div>
    );
};

export default connect(Nav);

const NavWrapper = styled.ul`
    display: none;
    margin: 0 20px 0 0;
    padding: 0;
    list-style: none;
    justify-content: end;
    @media (min-width: 992px) {
        display: flex;
        li {
            padding-left: 25px;
        }
    }
`;

const NavButton = styled.span`
    display: none;
    padding: 1.6rem;
    @media (min-width: 992px) {
        display: flex;
    }
    &.nav--item_custom-1 {
        background: #66DDF5;
    }
    &.nav--item_custom-2 {
        background: #00C7EE;
    }
    a {
        font-size: 16px;
        line-height: 19px;
        text-align: center;
        letter-spacing: 0.05em;
        color: var(--colour-blue-00);
        text-transform: uppercase;
        text-decoration: none;
        font-family: var(--font-bold);
    }
`;