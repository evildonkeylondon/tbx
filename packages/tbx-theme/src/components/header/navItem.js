import React, { useEffect } from 'react';
import { connect, styled } from 'frontity';
import Link from '../ui/link';

const NavItems = ({ items, state, libraries, actions }) => {

    let featuredServiceData = null;

    useEffect(() => {
        actions.source.fetch('/industries');
        actions.source.fetch('/solutions');
        actions.source.fetch('/services');
    }, []);

    const industriesPage = state.source.get('/industries');
    const industriesList = industriesPage.items;
    const solutionsPage = state.source.get('/solutions');
    const solutionsList = solutionsPage.items;
    const servicesPage = state.source.get('/services');
    const servicesList = servicesPage.items;

    const options = state.source.get("theme-tbx-settings");
    const { featured_service } = options.acf;
    if (servicesPage.isReady) {
        featuredServiceData = state.source[featured_service.post_type][featured_service.ID];
    }

    return (industriesPage.isReady && solutionsPage.isReady && servicesPage.isReady) ? (
        <>
            {items && items.map((item) => {
                const name = item.title;

                let subItems = false;
                let expertise = false;
                let industries = false;

                if (name == "Expertise") {
                    subItems = true;
                    expertise = true;
                }
                if (name == "Industries") {
                    subItems = true;
                    industries = true;
                }

                const link = libraries.source.normalize(item.url);
                const isCurrentPage = state.router.link === link;
                
                return (
                    <NavItem key={item.ID} isSelected={isCurrentPage} className={subItems ? "nav--arrow" : null}>
                        <Link link={link} aria-current={isCurrentPage ? "page" : null} className="menu--nav-item" children={name} />
                        {subItems && <svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 1.5L6 6.5L1 1.5" stroke="#66DDF5" strokeWidth="2"/></svg>}

                        {subItems && expertise && <div className="subnav--wrapper expertise--nav flex-column">
                            <div className="row">
                                {featuredServiceData &&
                                    <div className="col-md-3">
                                        <h5>Featured service</h5>
                                        <div className="featured--item">
                                            <h3>{featuredServiceData.title.rendered}</h3>
                                            <Link link={featuredServiceData.link} className="d-flex align-items-center mb-3">
                                                <span className="me-2">Read more</span>
                                                <svg width="9" height="17" viewBox="0 0 9 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.99991 16L6.33398 8.49989L1.99991 1" stroke="#00C7EE" strokeWidth="3" strokeMiterlimit="10"/></svg>
                                            </Link>
                                        </div>
                                    </div>
                                }
                                <div className="col-md-3">
                                    <h5>Solutions</h5>
                                    <ul>
                                        {solutionsList && solutionsList.map((item, i) => {
                                            const post = state.source[item.type][item.id];
                                            const name = post.title.rendered;
                                            const link = post.link;
                                            return (
                                                <li key={i.toString()}><Link link={link} children={name} /></li>
                                            )
                                        })}
                                    </ul>
                                </div>
                                <div className="col-md-6">
                                    <h5>Services</h5>
                                    <ul className="row">
                                        {servicesList && servicesList.map((item, i) => {
                                            const post = state.source[item.type][item.id];
                                            const name = post.title.rendered;
                                            const link = post.link;
                                            const { icon } = post.acf;
                                            return (
                                                <li key={i.toString()} className="col-md-6">
                                                    <Link link={link} className="d-flex align-items-center mb-3">
                                                        {icon && <figure className="d-flex me-2"><img src={icon} alt={name} /></figure>}
                                                        <span className="me-2">{name}</span>
                                                    </Link>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                            </div>
                        </div>}

                        {subItems && industries && <div className="subnav--wrapper industries--nav flex-column">
                            <h5>Industries</h5>
                            <ul>
                                {industriesList && industriesList.map((item, i) => {
                                    const post = state.source[item.type][item.id];
                                    const name = post.title.rendered;
                                    const link = post.link;
                                    return (
                                        <li key={i.toString()}><Link link={link} children={name} /></li>
                                    )
                                })}
                            </ul>
                        </div>}
                    </NavItem>
                )
            })}
        </>
    ) : null;
};

export default connect(NavItems);

const NavItem = styled.li`
    display: flex;
    align-items: center;
    position: relative;
    padding-top: 1rem;
    padding-bottom: 1rem;
    &.nav--arrow {
        a {
            padding-right: 10px;
        }
        svg {
            path {
                transition: var(--transition-normal);
                stroke: var(--colour-white-00);
            }
        }
        &:hover {
            &:before {
                content: "";
                width: 0;
                height: 0;
                position: absolute;
                bottom: 0;
                left: 50%;
                transform: translateX(-50%);
                border-style: solid;
                border-width: 0 5px 8.7px 5px;
                border-color: transparent transparent var(--colour-blue-01) transparent;
            }
            .subnav--wrapper {
                display: flex;
            }
            svg {
                path {
                    stroke: var(--colour-blue-01);
                }
            }
            > a {
                color: var(--colour-blue-01);
            }
        }
    }
    > a {
        color: var(--colour-white-00);
        text-decoration: none;
        transition: var(--transition-normal);
        font-size: var(--font-size-nav);
        font-family: var(--font-bold);
        &:hover {
            color: var(--colour-blue-01);
        }
    }
    .featured--item {
        h3 {
            font-size: 24px;
            line-height: 30px;
            color: var(--colour-white-00);
            margin-bottom: 20px;
        }
        a {
            text-decoration: none;
            color: var(--colour-blue-01);
        }
        span {
            font-family: var(--font-bold);
            font-size: 16px;
            line-height: 16px;
            text-transform: uppercase;
            transition: all .3s ease;
            letter-spacing: 0.05em;
        }
    }
    .subnav--wrapper {
        padding: 50px;
        top: 70px;
        display: none;
        background-color: var(--colour-blue-00);
        border-top: 1px solid #697780;
        &.expertise--nav {
            left: 0;
            position: fixed;
            min-width: 100vw;
        }
        &.industries--nav {
            left: 50%;
            position: absolute;
            transform: translateX(-50%);
            min-width: 300px;
        }
        h5 {
            font-size: 14px;
            line-height: 24px;
            margin-bottom: 10px;
        }
        ul {
            list-style: none;
            margin: 0;
            padding: 0;
            li {
                padding: 0;
                a {
                    font-size: 16px;
                    line-height: 22px;
                    font-family: var(--font-regular);
                    color: var(--colour-white-00);
                    text-decoration: none;
                    transition: var(--transition-normal);
                    &:hover {
                        color: var(--colour-blue-01);
                    }
                }
            }
        }
    }
`;