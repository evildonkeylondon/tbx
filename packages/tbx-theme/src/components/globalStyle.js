import { css } from "frontity";

import RoobertLight from '../assets/fonts/Roobert-Light.woff';
import RoobertRegular from '../assets/fonts/Roobert-Regular.woff';
import RoobertSemiBold from '../assets/fonts/Roobert-SemiBold.woff';
import RoobertMedium from '../assets/fonts/Roobert-Medium.woff';
import RoobertBold from '../assets/fonts/Roobert-Bold.woff';

const styleWithFont = css`
    @font-face {
        font-family: "Roobert Regular";
        font-style: normal;
        font-weight: 400;
        src: url(${RoobertRegular});
    }
    @font-face {
        font-family: "Roobert SemiBold";
        font-style: normal;
        font-weight: 400;
        src: url(${RoobertSemiBold});
    }
    @font-face {
        font-family: "Roobert Bold";
        font-style: normal;
        font-weight: 400;
        src: url(${RoobertBold});
    }
    @font-face {
        font-family: "Roobert Medium";
        font-style: normal;
        font-weight: 400;
        src: url(${RoobertMedium});
    }
    @font-face {
        font-family: "Roobert Light";
        font-style: normal;
        font-weight: 400;
        src: url(${RoobertLight});
    }

    :root {
        --font-light: "Roobert Light";
        --font-regular: "Roobert Regular";
        --font-medium: "Roobert Medium";
        --font-semibold: "Roobert SemiBold";
        --font-bold: "Roobert Bold";

        --font-size-nav: 16px;
        --font-size-p: 18px;
        --font-size-quote: 26px;

        --colour-white-00: #ffffff;
        --colour-black-00: #000000;
        --colour-blue-00: #051C2C;
        --colour-blue-01: #66DDF5;
        --colour-blue-02: #697780;
        --colour-blue-03: #374956;
        --colour-grey-00: #F3F4F4;
        --colour-grey-01: #E6E8EA;
        --break-small: 468px;
        --break-medium: 992px;
        --break-medium: 1200px;

        --ease-in: cubic-bezier(0.4, 0, 1, 1);
        --ease-out: cubic-bezier(0, 0, 0.2, 1);
        --ease-in-out: cubic-bezier(0.4, 0, 0.2, 1);
        --ease: var(--ease-in-out);
        --transition-normal: all .3s ease-out;
    }

    @media not all and (min-resolution:.001dpcm) { 
        @supports (-webkit-appearance:none) {
            html,
            body {
                position: relative;
                overflow-x: hidden;
            }
        }
    }
    body {
        cursor: default;
        margin: 0;
        overflow-x: hidden;
        font-family: var(--font-regular);
        -webkit-font-smoothing:antialiased;
	    -moz-osx-font-smoothing:grayscale;
        font-size: 26px;
        line-height: 38px;
    }
    #root {
        display: flex;
        flex-direction: column;
        min-height: 100vh;
    }
    figure {
        margin: 0;
    }
    img {
        max-width: 100%;
    }
    b,
    strong {
        font-family: var(--font-bold);
    }

    h1 {
        font-size: 45px;
        line-height: 50px;
        font-family: var(--font-light);
        .hero--homepage & {
            @media (min-width: 768px) {
                font-size: 70px;
                line-height: 78px;
            }
        }
    }
    h2 {
        font-size: 45px;
        line-height: 53px;
        margin-bottom: 30px;
    }
    h3 {
        font-family: var(--font-medium);
        font-size: 30px;
        line-height: 30px;
    }
    h4 {
        font-family: var(--font-semibold);
        font-size: 26px;
        line-height: 35px;
    }
    h5 {
        font-family: var(--font-bold);
        font-size: 18px;
        line-height: 18px;
        margin-bottom: 10px;
        color: var(--colour-blue-02);
    }

    p {
        font-size: var(--font-size-p);
        line-height: 24px;
    }

    blockquote p {
        font-size: var(--font-size-quote);
        line-height: 38px;
    }

    

    .border-t {
        &:before {
            content: "";
            width: calc(100% - 30px);
            height: 1px;
            position: absolute;
            left: 15px;
            top: 0;
            background-color: var(--grey-0);
        }
    }
    .border-r {
        @media (min-width: 768px) {
            &:before {
                content: "";
                height: 100%;
                width: 1px;
                position: absolute;
                right: 0;
                top: 0;
                background-color: var(--grey-0);
            }
        }
    }

    .fill__bg-image {
        width: 100%;
        height: 100%;
        background-size: cover;
        background-position: center center;
    }

    .wpcf7-form {
        label,
        .wpcf7-form-control-wrap {
            width: 100%;
            display: inline-block;
        }
        .form--label {
            font-size: 18px;
            line-height: 18px;
            font-family: var(--font-bold);
            color: var(--colour-blue-02);
            display: inline-block;
            width: 100%;
            margin-bottom: 15px;
            margin-top: 30px;
        }
        small {
            display: inline-block;
            font-size: 14px;
            line-height: 19px;
            margin-top: 15px;
            margin-bottom: 30px;
        }
        button {
            background-color: transparent;
            font-family: var(--font-bold);
            padding: 12px;
            color: inherit;
            color: var(--colour-blue-00);
            border: 2px solid var(--colour-blue-01);
            font-size: 16px;
            line-height: 16px;
            text-transform: uppercase;
            text-decoration: none !important;
            transition: all .3s ease;
            word-spacing: .05em;
            scroll-behaviour: smooth;
            max-width: 200px;
            width: 100%;
            &:hover {
                color: var(--colour-blue-00);
                background-color: var(--colour-blue-01);
                svg {
                    transform: translateX(2px);
                    path {
                        stroke: var(--colour-blue-00);
                    }
                }
            }
            svg {
                margin-left: 20px;
                transition: var(--transition-normal);
                path {
                    transition: var(--transition-normal);
                }
            }
        }
        .error-message {
            border: none;
            padding: 0;
            margin-top: 30px;
            font-size: 21px;
        }
    }


    input[type="text"],
    input[type="email"],
    textarea,
    select {
        width: 100%;
        border-radius: 0;
        background-color: var(--colour-white-00);
        border: 2px solid var(--colour-grey-01);
        font-size: 18px;
        line-height: 16px;
        padding: 20px;
    }
    select {
        outline: none !important;
        width: 49%;
        background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEAkACQAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAPACIDAREAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9Sf27/wBoH9u34Cftma/8YJW8ZfCTQpJh4X+F1tDdwa/8NvFHw90J7pLC11C2SbU/CmuXutfab7xPrGkanGdb0HVdYeW2h0q4sdNntvosHRwdbCxpe5VfxVNOWpGb3a2kktIprRpdbs+ZxtfG0MXKr79KN+Wmr81OUI3tfeMnLWTT96LelrJn6Ffskf8ABZH4SfFn+z/B37QdrY/Bnx7Mbe0tfEyTXdz8NfEl3K5QE30sct34JuGym6LxBPdaIAryHxJDJLDYLxYnK6lO8qN6sN+XT2kV6bT/AO3df7vU78Lm1KraFdKjPbm3pyfrvB+UtP73Q+l/2qv+Cl/7OP7MFncaW2vQfFH4lPaNNp3w/wDAeo2OoPbyyQCWzk8WeIopLjSvCtlN5kDskpv9ee0njvbHw/fWx8wc+HwFfEO9vZ076zmmvXljvJ/cr6OSOjE5jh8Ndc3tKltKcGnZ9OeW0V98raqLP56PG/7dH7cn7ZHxj8JW3w7uPEtrqOi+JdJ8TeAvhP8ACSy1BtH07UvD1/Be6drOvQhri48R/wBnzpBc6pqvi25l0CyUzyraaPp0sluvtwwmEwtKXPy2cXGdSq1dqSs0v5b7JR9592zwZ43G4urBU+ZNSUoUqSdk4u6cusraXcnyrV2ij+tbwf4j+JV74S8LXnjL4dLo3i+78OaJc+K9IsPEuhXljpXiSfTLWXXdNsryO7mju7Sx1R7q1trmOWVJ4YklWR1YMfm5RpqUlGpeKk+VuMk3G+jatu0fUQlUcYudO03FcyUotKVlzJPqk7q51PjHwX4S+IXhrVvB3jnw5o3i3wtrtq9nq+ga/p9tqel31u/O2a1uo5I98bhZbedQs9tOkdxbyRTxxyLMZyhJShJxktVKLs180XOEakXCcVKMlZxkrpo/Ab9rb/gidbXX9peNf2SNXSzuC8lzN8HPF+pv9jlV2lkaLwb411O4d7V4yYYLXSPF0rwSIZZ5vFtsYY7W49nDZrtDEq//AE9itf8At6C/OP8A4D1PDxWTp3nhXZ7+ym9P+3JvbyUv/Aj5V/Y+/wCCQXxP+N4sPG/xm1y2+GPwwN7NGtjomoaP4g8deKYrG6mtL2LSmsZtT0Hw5bC5t5rb+1NXkv7tJELw+Hbu3dLiujE5nTpXhSXtKlt2nGEbq6ve0pd7Ky/vI5cJlVSvadWXs6V9otSnK107WvGOq3d3/dZ/St8B/wBm34L/ALNfhWPwl8HvA2k+FrRo4l1XV1iW78T+JJ4izC78SeI7kPqusTiSSR4I7m4NnYLIbfTbWytFjt08KtXq15c1Wbl2W0Y+UYrRfm+rbPoqGHo4ePJSgorq95S85S3f5Lokj3OsTYD/2Q==');
        background-position: right 20px center;
        background-repeat: no-repeat;
        background-size: 20px;
        appearance: none;
        &.news--dropdown {
            width: 100%;
            @media screen and (min-width: 992px) {
                width: auto;
            }
        }
    }


    .container {
        padding-left: 40px;
        padding-right: 40px;
        @media screen and (min-width: 992px) {
            padding-left: 12px;
            padding-right: 12px;
        }
        .row {
            margin-left: -40px;
            margin-right: -40px;
            @media screen and (min-width: 992px) {
                margin-left: -12px;
                margin-right: -12px;
            }
            > * {
                padding-left: 40px;
                padding-right: 40px;
                @media screen and (min-width: 992px) {
                    padding-left: 12px;
                    padding-right: 12px;
                }
            }
        }
    }

`;

export default styleWithFont;