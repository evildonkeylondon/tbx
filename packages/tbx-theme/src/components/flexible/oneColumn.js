import React from 'react';
import { connect, styled } from 'frontity';
import Column from '../column';

const OneColumn = ({ content }) => {
    const { copy } = content;
    return (
        <>
            {copy && <div className="container my-5 py-md-3">
                <div className="row justify-content-center">
                    <div className="col-md-9">
                        <Column content={copy} />
                    </div>
                </div>    
            </div>}
        </>
    );
}

export default connect(OneColumn);