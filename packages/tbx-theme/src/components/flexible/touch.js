import React from 'react';
import { connect, styled } from "frontity";
import Link from '../ui/link';

const Touch = ({ state, content, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const { main_copy, box_copy, box_image, url } = content;

    return (
        <TouchWrapper>
            <div className="container d-flex flex-grow-1">
                <div className="row w-100 flex-grow-1 align-items-center justify-content-between">
                    <div className="col-md-5 py-5 my-5">
                        {main_copy && <Html2React html={main_copy} />}
                    </div>
                    {box_copy && <div className="col-md-6 align-self-end">
                        <SideBox className="d-flex">
                            {box_image && <img src={box_image} />}
                            <div className="p-4 d-flex flex-column justify-content-between">
                                <p>
                                    <Html2React html={box_copy} />
                                </p>
                                <Link link={url} className="d-flex align-items-center">
                                    <span className="me-2">Get in touch</span>
                                    <svg width="9" height="17" viewBox="0 0 9 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.99991 16L6.33398 8.49989L1.99991 1" stroke="#00C7EE" strokeWidth="3" strokeMiterlimit="10"/></svg>
                                </Link>
                            </div>
                        </SideBox>
                    </div>}
                </div>
            </div>
        </TouchWrapper>
    );
}

export default connect(Touch);

const TouchWrapper = styled.div`
    width: 100%;
    display: flex;
    background-color: var(--colour-blue-00);
    position: relative;
    color: var(--colour-white-00);
    z-index: 1;
    h2,
    h3 {
        color: var(--colour-blue-01);
    }
`;

const SideBox = styled.div`
    background-color: var(--colour-blue-01);
    font-size: 18px;
    line-height: 26px;
    transform: translateY(30px);
    color: var(--colour-blue-00);
    img {
        max-width: 190px;
    }
    svg {
        path {
            stroke: var(--colour-blue-00);
        }
    }
    p {
        font-family: var(--font-bold);
        font-size: 21px;
        line-height: 29px;
    }
    a {
        text-decoration: none;
        color: var(--colour-blue-00);
    }
    span {
        font-family: var(--font-bold);
        font-size: 16px;
        line-height: 16px;
        text-transform: uppercase;
        transition: all .3s ease;
        letter-spacing: 0.05em;
    }
`;