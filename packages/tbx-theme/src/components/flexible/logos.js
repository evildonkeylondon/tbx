import React from 'react';
import { connect, styled } from 'frontity';

const Logos = ({ content, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const { logos, copy } = content;
    return (
        <LogosWrapper className="container mb-5 position-relative py-5">
            <div className="row align-items-center">
                <div className="col-md-6 d-flex p-md-5 flex-column justify-content-center">
                    <div className="p-4 p-md-5">
                        <div>
                            <Html2React html={copy} />
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="row align-items-center justify-content-center">
                        {logos && logos.map((item, i) => {
                            return (
                                <figure key={i.toString()} className="col-6 col-md-4 p-3">
                                    <img src={item.logo} />
                                </figure>
                            );
                        })}
                    </div>
                </div>
            </div>
        </LogosWrapper>
    );
}

export default connect(Logos);

const LogosWrapper = styled.div`
    background-color: var(--colour-blue-00);
    color: var(--colour-white-00);
    h2 {
        color: var(--colour-blue-01);
    }
`;