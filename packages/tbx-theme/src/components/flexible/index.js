import React from 'react';
import { connect } from 'frontity';
import OneColumn from './oneColumn';
import TextImage from './textImage';
import ServicesTabs from './servicesTabs';
import Industries from './industries';
import Posts from './posts';
import ArrowBanner from './arrowBanner';
import BigBanner from './bigBanner';
import BannerList from './bannerList';
import DataBlocks from './dataBlocks';
import Logos from './logos';
import Touch from './touch';
import Team from './team';
import FullScreenImage from './fullScreenImage';


const FlexibleContent = ({ flexibleContent }) => {
    return (
        <>
            {flexibleContent && flexibleContent.map((content, index) => {
                if (content.acf_fc_layout === "one_column")
                    return <OneColumn key={index} content={content} />;
                
                if (content.acf_fc_layout === "text_w_image")
                    return <TextImage key={index} content={content} />;

                if (content.acf_fc_layout === "full_screen_image")
                    return <FullScreenImage key={index} content={content} />;

                if (content.acf_fc_layout === "services_tabs")
                    return <ServicesTabs key={index} content={content} />;

                if (content.acf_fc_layout === "industries")
                    return <Industries key={index} content={content} />;

                if (content.acf_fc_layout === "posts")
                    return <Posts key={index} content={content} />;

                if (content.acf_fc_layout === "arrow_banner")
                    return <ArrowBanner key={index} content={content} />;

                if (content.acf_fc_layout === "big_banner_with_side_content")
                    return <BigBanner key={index} content={content} />;

                if (content.acf_fc_layout === "banner_with_list")
                    return <BannerList key={index} content={content} />;

                if (content.acf_fc_layout === "data_blocks")
                    return <DataBlocks key={index} content={content} />;

                if (content.acf_fc_layout === "logos_block")
                    return <Logos key={index} content={content} />;

                if (content.acf_fc_layout === "get_in_touch")
                    return <Touch key={index} content={content} />;

                if (content.acf_fc_layout === "team")
                    return <Team key={index} content={content} />;

            })}
        </>
    );
}
        
export default connect(FlexibleContent);