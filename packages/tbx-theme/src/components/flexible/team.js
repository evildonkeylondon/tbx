import React from 'react';
import { connect, styled } from 'frontity';

const Team = ({ content }) => {
    const { heading, team } = content;
    return (
        <div className="container my-5">
            {heading && <div className="row mt-5">
                <div className="col">
                    <p>{heading}</p>
                </div>
            </div>}
            {team && <TeamList className="row list-unstyled p-0 mb-0 mt-4">
                {team.map((item, i) => {
                    const { name, job_title, photo } = item;
                    return (
                        <li key={i.toString()} className="col-md-3 mb-4">
                            <figure><img src={photo.sizes.team_size} alt={name} /></figure>
                            <p><span>{name}</span><br/>
                            {job_title}</p>
                        </li>
                    )
                })}
            </TeamList>}
        </div>
    );
}

export default connect(Team);

const TeamList = styled.ul`
    li {
        p {
            margin-top: 20px;
            span {
                font-family: var(--font-bold);
            }
        }
    }
`;