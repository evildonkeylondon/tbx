import React, { useEffect } from 'react';
import { connect, styled } from 'frontity';
import { TabContainer, TabContent, TabPane, Nav } from 'react-bootstrap';
import ListItems from '../listItems';

const ServicesTabs = ({ state, content, libraries, actions }) => {
    const Html2React = libraries.html2react.Component;
    const { heading, tabs } = content;

    const data = state.source.get('/services/');

    useEffect(() => {
        actions.source.fetch('/services/');
    }, []);

    return data.isReady ? (
        <div className="container my-5 position-relative">
            <div className="row">
                <div className="col">
                    <p>{heading}</p>
                </div>
            </div>
            <div className="row d-none d-md-block">
                <div className="col">
                    <TabContainer id="left-tabs__container" defaultActiveKey="0">
                        <TabContainerWrapper className="row mb-4">
                            <div className="col-md-8">
                                <Nav variant="pills" className="flex-column">
                                    {tabs && tabs.map((item, i) => {
                                        const { title } = item;
                                        return (
                                            <NavItem key={i.toString()}>
                                                <Nav.Link eventKey={i.toString()}><h2>{title}</h2></Nav.Link>
                                            </NavItem>
                                        )
                                    })}
                                </Nav>
                            </div>
                            <div className="col-md-4">
                                <TabContent>
                                    {tabs && tabs.map((item, i) => {
                                        const { services } = item;
                                        return (
                                            <TabPaneItem key={i.toString()} eventKey={i.toString()}>
                                                <ListItems items={services} />
                                            </TabPaneItem>
                                        )
                                    })}
                                </TabContent>
                            </div>
                        </TabContainerWrapper>
                    </TabContainer>
                </div>
            </div>
        </div>
    ) : null;
}

export default connect(ServicesTabs);

const TabContainerWrapper = styled.div`
    margin-top: 20px;
    margin-bottom: 20px;
    padding: 20px 0;
    border-top: 1px solid #E6E8EA;
    border-bottom: 1px solid #E6E8EA;
`;

const NavItem = styled(Nav.Item)`
    a {
        background-color: transparent !important;
        padding: 0;
        color: var(--colour-grey-01) !important;
        cursor: pointer;
        &.active {
            color: var(--colour-blue-00) !important;
        }
        h2 {
            margin: 0;
            line-height: 1.4;
        }
    }
`;

const TabPaneItem = styled(TabPane)`
    margin-top: 15px;
`;