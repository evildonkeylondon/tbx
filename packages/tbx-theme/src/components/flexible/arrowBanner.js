import React, { useRef, useEffect } from 'react';
import { connect, styled } from "frontity";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

const ArrowBanner = ({ state, content }) => {
    const { heading, copy } = content;
    let arrow1Ref = useRef();
    let arrow2Ref = useRef();
    let arrow3Ref = useRef();

    useEffect(() => {

        gsap.set(arrow1Ref.current, {x: -20});
        gsap.set(arrow2Ref.current, {x: -40});
        gsap.set(arrow3Ref.current, {x: -80});

        gsap.to(arrow1Ref.current, {
            duration: .5,
            x: 0,
            scrollTrigger: {
                id: "arrow1-ref",
                trigger: arrow1Ref.current,
                start: "top 70%",
                toggleActions: "play none none reverse",
                scrub: false
            }
        });

        gsap.to(arrow2Ref.current, {
            duration: .5,
            x: 0,
            scrollTrigger: {
                id: "arrow2-ref",
                trigger: arrow2Ref.current,
                start: "top 70%",
                toggleActions: "play none none reverse",
                scrub: false
            }
        });

        gsap.to(arrow3Ref.current, {
            duration: .5,
            x: 0,
            scrollTrigger: {
                id: "arrow3-ref",
                trigger: arrow3Ref.current,
                start: "top 70%",
                toggleActions: "play none none reverse",
                scrub: false
            }
        });
        
        return () => {
            const arrow1Animation = ScrollTrigger.getById(`arrow1-ref`);
            const arrow2Animation = ScrollTrigger.getById(`arrow2-ref`);
            const arrow3Animation = ScrollTrigger.getById(`arrow3-ref`);
			if (arrow1Animation) {
				arrow1Animation.kill(true);
			}
            if (arrow2Animation) {
				arrow2Animation.kill(true);
			}
            if (arrow3Animation) {
				arrow3Animation.kill(true);
			}
        }

    }, []);

    return (
        <BannerWrapper>
            <svg width="641" height="300" viewBox="0 0 641 300" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g ref={arrow1Ref}>
                    <path d="M321.023 -165L496.607 139.557H640.218L464.616 -165H321.023Z" fill="#66DDF5"/>
                    <path d="M496.607 139.557L321.023 444.123H464.616L640.218 139.557H496.607Z" fill="#00C7EE"/>
                </g>
                <g ref={arrow2Ref}>
                    <path d="M161.885 -165L337.469 139.557H481.08L305.478 -165H161.885Z" fill="#374956"/>
                    <path d="M337.469 139.557L161.885 444.123H305.478L481.08 139.557H337.469Z" fill="#243746"/>
                </g>
                <g ref={arrow3Ref}>
                    <path d="M0 -165L175.584 139.557H319.195L143.593 -165H0Z" fill="#374956"/>
                    <path d="M175.584 139.557L0 444.123H143.593L319.195 139.557H175.584Z" fill="#243746"/>
                </g>
            </svg>

            <div className="container position-relative">
                <div className="row">
                    <div className="col-10 col-md-6 offset-md-1">
                        <h2 className="mb-3">{heading}</h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-10 col-md-4 offset-md-1">
                        <p>{copy}</p>
                    </div>
                </div>
            </div>
        </BannerWrapper>
    );
}

export default connect(ArrowBanner);

const BannerWrapper = styled.div`
    width: 100%;
    display: inline-block;
    background-color: var(--colour-blue-00);
    padding: 50px 0;
    overflow: hidden;
    position: relative;
    color: var(--colour-white-00);
    svg {
        position: absolute;
        @media screen and (max-width: 991px) {
            max-width: 220px;
            height: 200px;
            right: 20px;
            bottom: 0;
        }
        @media screen and (min-width: 992px) {
            top: 50%;
            right: 50px;
            transform: translateY(-50%);
        }
    }
`;