import React from 'react';
import { connect, styled } from "frontity";
import ListItems from '../listItems';

const BannerList = ({ state, content, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const { copy, background_image, list_heading, list } = content;

    return (
        <BannerWrapper className="mb-5">
            {background_image && <figure><img src={background_image} /></figure>}
            <div className="container">
                <div className="row">
                    <div className="col-md-11">
                        <InnerBannerList>
                            <div className="row">
                                <div className="col-md-5">
                                    {copy && <Html2React html={copy} />}
                                </div>
                                <div className="col-md-5 offset-md-1">
                                    <p className="mb-4">{list_heading && <Html2React html={list_heading} />}</p>
                                    <ListItems items={list} />
                                </div>
                            </div>
                        </InnerBannerList>
                    </div>
                </div>
            </div>
        </BannerWrapper>
    );
}

export default connect(BannerList);

const BannerWrapper = styled.div`
    width: 100%;
    display: flex;
    position: relative;
    color: var(--colour-white-00);
    > figure {
        margin: 0;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        z-index: -1;
        overflow: hidden;
        max-height: 640px;
    }
`;

const InnerBannerList = styled.div`
    background-color: var(--colour-white-00);
    padding: 50px;
    color: var(--colour-blue-00);
    margin-top: 200px;
`;