import React from 'react';
import { connect, styled } from "frontity";
import Cta from '../cta';

const BigBanner = ({ state, content, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const { main_copy, side_copy, background_image, cta } = content;

    return (
        <BannerWrapper bg={background_image}>
            <div className="container d-flex flex-grow-1">
                <div className="row w-100 flex-grow-1 align-items-center justify-content-between">
                    <div className="col-md-5 py-5 offset-md-1">
                        {main_copy && <Html2React html={main_copy} />}
                        {cta && <Cta cta={cta} className="pt-4" />}
                    </div>
                    {side_copy && <div className="col-md-4 align-self-end p-0 px-md-5">
                        <SideBox className="p-5 p-md-4">
                            <Html2React html={side_copy} />
                        </SideBox>
                    </div>}
                </div>
            </div>
        </BannerWrapper>
    );
}

export default connect(BigBanner);

const BannerWrapper = styled.div`
    width: 100%;
    display: flex;
    background-color: var(--colour-blue-00);
    min-height: 75vh;
    position: relative;
    color: var(--colour-white-00);
    background-image: ${props => props.bg ? `url(${props.bg})` : null};
    background-size: cover;
    background-position: center center;
`;

const SideBox = styled.div`
    background-color: var(--colour-blue-01);
    font-size: 18px;
    line-height: 26px;
    color: var(--colour-blue-00);
    @media screen and (min-width: 992px) {
        transform: translateY(30px);
    }
    h4 {
        margin-bottom: 20px;
    }
    h5 {
        color: var(--colour-blue-03);
        font-size: 16px;
        line-height: 16px;
    }
`;