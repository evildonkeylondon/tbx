import React from 'react';
import { connect, styled } from 'frontity';

const DataBlocks = ({ content, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const { blocks } = content;
    return (
        <div className="container-fluid p-0 mb-5">
            <div className="row m-0">
                {blocks && blocks.map((item, i) => {
                    const { data, description, background_colour } = item;
                    return (
                        <Block key={i.toString()} className="col-12 col-sm-6 col-md-3 p-4 d-flex flex-column justify-content-between" bg={background_colour}>
                            <span>{data}</span>
                            <div><Html2React html={description} /></div>
                        </Block>
                    )
                })}
            </div>
        </div>
    );
}

export default connect(DataBlocks);

const Block = styled.div`
    background-color: var(${props => props.bg});
    min-height: 480px;
    span {
        font-size: 115px;
        line-height: 138px;
        color: ${props => props.bg === "--colour-blue-00" ? "var(--colour-blue-01)" : "var(--colour-blue-00)"};
    }
    div {
        color: ${props => props.bg === "--colour-blue-00" ? "var(--colour-white-00)" : "var(--colour-blue-00)"};
    }
`;