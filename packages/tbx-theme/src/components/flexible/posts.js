import React from 'react';
import { connect, styled } from 'frontity';
import PostsList from "../posts";
import Button from '../ui/button';

const Posts = ({ content }) => {
    const { heading, posts } = content;

    return (
        <PostsWrapper>
            <div className="container mt-5 position-relative">
                <div className="row mb-4">
                    <div className="col-md-3">
                        <h5>{heading}</h5>
                    </div>
                </div>
                {posts && <PostsList items={posts} />}
                <CTAbtn className="row justify-content-center mb-5 mt-1 mt-md-0">
                    <div className="col-auto">
                        <Button
                            link="/insights" 
                            text="View all insights"
                            colour="light"
                        />
                    </div>
                </CTAbtn>
            </div>
        </PostsWrapper>
    );
}

export default connect(Posts);

const PostsWrapper = styled.div`
    display: inline-block;
    width: 100%;
    background: linear-gradient(180deg, #FFFFFF 0.01%, #F3F4F4 100%);
`;

const CTAbtn = styled.div`
    margin-top: -100px;
    position: relative;
    z-index: 10;
`;