import React from 'react';
import { connect } from 'frontity';

const FullScreenImage = ({ content }) => {
    const { image } = content;
    return (
        <>
            {image && <img src={image} />}
        </>
    );
}

export default connect(FullScreenImage);