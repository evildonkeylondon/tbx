import React from 'react';
import { connect, styled } from 'frontity';
import ListItems from '../listItems';
import Cta from '../cta';

const TextImage = ({ content, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const { image, image_order, copy, cta, theme, heading, links } = content;
    return (
        <div className="container mb-5 position-relative">
            <div className="row">
                <ImageCol className={`${heading ? "col-md-5" : "col-md-6" } ${image_order}`} image={image} padding={image_order}>
                    {!heading && <div />}
                    {heading && <Html2React html={`<h2>${heading}</h2>`} />}
                </ImageCol>
                <TextCol className={`col-md-6 d-flex ${theme} ${!heading ? 'p-md-5 flex-column justify-content-center' : 'offset-md-1'}`} padding={image_order}>
                    <div className={`${!heading ? 'py-5 p-md-5' : ''}`}>
                        <div>
                            <Html2React html={copy} />
                        </div>
                        {cta && <Cta cta={cta} className="pt-4" />}
                        {links && <ListItems items={links} />}
                    </div>
                </TextCol>
            </div>
        </div>
    );
}

export default connect(TextImage);

const TextCol = styled.div`
    &.grey {
        background-color: var(--colour-grey-00);   
    }
    &.blue-0 {
        background-color: var(--colour-blue-00);
        color: var(--colour-white-00);
        h2 {
            color: var(--colour-blue-01);
        }
    }
    &.blue-1 {
        background-color: var(--colour-blue-01);   
    }
`;

const ImageCol = styled.div`
    min-height: ${props => props.image ? "400px" : ""};
    @media screen and (max-width: 991px) {
        padding: 0 !important;
    }
    @media screen and (min-width: 992px) {
        min-height: ${props => props.image ? "600px" : ""};
        padding-right: ${props => (props.padding === "order-md-first") ? "0" : null};
        padding-left: ${props => (props.padding === "order-md-last") ? "0" : null};
    }
    div {
        background-image: url(${props => props.image});
        background-size: cover;
        background-position: center center;
        width: 100%;
        height: 100%;
    }
`;