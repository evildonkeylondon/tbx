import React, { useEffect } from 'react';
import { connect, styled } from 'frontity';
import Link from "../ui/link";

const Posts = ({ state, actions, content }) => {
    const { industries } = content;
    
    const data = state.source.get('/industries/');
    // const { items } = data;

    useEffect(() => {
        actions.source.fetch('/industries/');
    }, []);

    return data.isReady ? (
        <div className="container my-5 position-relative">
            {industries && 
                <IndustriesList className="row p-0 mb-0 list-unstyled">
                    {industries.map((item, i) => {
                        const { post_type, ID } = item;
                        const postData = state.source[post_type][ID];
                        const { link } = postData;
                        const title = postData.title.rendered;
                        const { short_description, icon } = postData.acf;
                        return (
                            <li key={i.toString()} className="col-12">
                                <Link link={link} className="row align-items-center">
                                    {icon &&  <figure><img src={icon} alt={title} /></figure>}
                                    <h2 className="m-md-0 col-md-5">{title}</h2>
                                    {short_description && <p className="m-0 col-md-5">{short_description}</p>}
                                    <svg width="14" height="34" viewBox="0 0 14 34" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.36876 16.9997L0.000592232 34H6.02637L13.3945 16.9997L6.02637 0H0.000592232L7.36876 16.9997Z" fill="#66DDF5"/></svg>
                                </Link>
                            </li>
                        )
                    })}
                </IndustriesList>
            }
        </div>
    ) : null;
}

export default connect(Posts);

const IndustriesList = styled.ul`
    li {
        position: relative;
        a {
            padding-top: 11px;
            padding-bottom: 11px;
            color: var(--colour-blue-00);
            text-decoration: none;
            transition: var(--transition-normal);
        }
        &:hover {
            a {
                padding-top: 22px;
                padding-bottom: 22px;
            }
            &:before,
            p,
            figure {
                opacity: 1;
                visibility: visible;
            }
            figure {
                max-width: 50px;
                margin-right: 15px;
            }
            svg path {
                fill: var(--colour-blue-00);
            }
        }
        &:before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 200vw;
            transform: translateX(-50%);
            background-color: var(--colour-grey-00);
            transition: var(--transition-normal);
            z-index: -1;
            opacity: 0;
            visibility: hidden;
        }
        svg {
            position: absolute;
            right: 15px;
            top: 50%;
            width: auto;
            transform: translateY(-50%);
            path {
                transition: var(--transition-normal);
            }
        }
        p,
        figure {
            opacity: 0;
            visibility: hidden;
            transition: var(--transition-normal);
        }
        figure {
            max-width: 0;
            padding: 0;
            overflow: hidden;
        }
    }
`;