import React, { useEffect, useState } from 'react';
import { connect, styled } from 'frontity';
import dayjs from "dayjs";
import Link from "../ui/link";

const Grid = ({ state, items, single, featured, libraries }) => {
    const Html2React = libraries.html2react.Component;

    let gridItems = items;
    let featuredItems = gridItems.slice(0, 2);
    let smallGridItems = gridItems.slice(2);
    // let filterId = state.theme.selectedFilterId;
    // let filterType = state.theme.selectedFilterType;
    let storedGridItems = state.theme.storedGridItems;

    // if (filterId != "*") {
    //     gridItems = gridItems.filter(function (a) {
    //         if (a.acf.industries && filterType == "industry") {
    //             return a.acf.industries.some(function (b) {
    //                 return b.ID == filterId;
    //             });
    //         }
    //         if (a.acf.services && filterType == "service") {
    //             return a.acf.services.some(function (b) {
    //                 return b.ID == filterId;
    //             });
    //         }
    //         if (a.acf.solutions && filterType == "solution") {
    //             return a.acf.solutions.some(function (b) {
    //                 return b.ID == filterId;
    //             });
    //         }
    //     });
    // }


    if (storedGridItems.filtered) {
        if (storedGridItems.industry) {
            featuredItems = featuredItems.filter(function (a) {
                if (a.acf.industries) {
                    return a.acf.industries.some(function (b) {
                        return b.ID == storedGridItems.industry;
                    });
                }
            });

            gridItems = gridItems.filter(function (a) {
                if (a.acf.industries) {
                    return a.acf.industries.some(function (b) {
                        return b.ID == storedGridItems.industry;
                    });
                }
            });
        }

        if (storedGridItems.service) {
            featuredItems = featuredItems.filter(function (a) {
                if (a.acf.services) {
                    return a.acf.services.some(function (b) {
                        return b.ID == storedGridItems.service;
                    });
                }
            });

            gridItems = gridItems.filter(function (a) {
                if (a.acf.services) {
                    return a.acf.services.some(function (b) {
                        return b.ID == storedGridItems.service;
                    });
                }
            });
        }

        if (storedGridItems.solution) {
            featuredItems = featuredItems.filter(function (a) {
                if (a.acf.solutions) {
                    return a.acf.solutions.some(function (b) {
                        return b.ID == storedGridItems.solution;
                    });
                }
            });
            
            gridItems = gridItems.filter(function (a) {
                if (a.acf.solutions) {
                    return a.acf.solutions.some(function (b) {
                        return b.ID == storedGridItems.solution;
                    });
                }
            });
        }

        // gridItems = gridItems.filter(function (a) {
        //     if (a.acf.industries && filterType == "industry") {
        //         return a.acf.industries.some(function (b) {
        //             return b.ID == filterId;
        //         });
        //     }
        //     if (a.acf.services && filterType == "service") {
        //         return a.acf.services.some(function (b) {
        //             return b.ID == filterId;
        //         });
        //     }
        //     if (a.acf.solutions && filterType == "solution") {
        //         return a.acf.solutions.some(function (b) {
        //             return b.ID == filterId;
        //         });
        //     }
        // });
    }


    return (
        <div className="row">
            {gridItems.map((item, i) => {
                const { post_type, type, ID, id } = item;

                const postData = state.source[post_type ? post_type : type][ID ? ID : id];
                const { categories, featured_media, link, events_categories } = postData;
                const title = postData.title.rendered;
                const excerpt = postData.excerpt.rendered;
                const fmedia = state.source.attachment[featured_media];
                let source_url = null;
                if (fmedia !== undefined && fmedia !== null) {
                    source_url = fmedia.source_url;
                }
                const { details } = postData.acf;
                const formattedDate = dayjs(postData.date).format("DD MMMM YYYY");
                const category = categories ? state.source['category'][categories[0]] : state.source['events_categories'][events_categories[0]];
                const { name } = category;
                return (
                    <PostItem key={i.toString()} className={`grid--item ${single ? "col" : featured ? "col-md-6" : "col-md-4"}`}>
                        <div>
                            {source_url && <Link link={link}><img src={source_url} alt={title} /></Link>}
                        </div>
                        <div className="post--item-wrapper">
                            <div className="post--meta px-4 py-3">
                                <b>{name} </b>{formattedDate}
                            </div>
                            <div className="post--content position-relative">
                                {title && <Link link={link}><h4>{title}</h4></Link>}
                                {excerpt && <Html2React html={excerpt} />}
                                {details && <h5>{details}</h5>}
                                {link && excerpt && <Link link={link} className="d-flex align-items-center read--more-btn mt-4">
                                    <span className="me-2">Read more</span>
                                    <svg width="9" height="17" viewBox="0 0 9 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.99991 16L6.33398 8.49989L1.99991 1" stroke="#00C7EE" strokeWidth="3" strokeMiterlimit="10"/></svg>
                                </Link>}
                            </div>
                        </div>
                    </PostItem>
                );
            })}
    </div>
    );
}

export default connect(Grid);

const PostItem = styled.div`
    &.grid--item {
        transition: var(--transition-normal);
        &:hover {
            transform: translateY(-15px);
        }   
    }
    .post--item-wrapper {
        &:hover {
            .post--meta {
                background-color: var(--colour-blue-01);
            }
        }
    }
    .post--meta {
        background-color: var(--colour-grey-01);
        transition: var(--transition-normal);
        font-size: 16px;
        line-height: 16px;
    }
    .post--content {
        background-color: var(--colour-white-00);
        padding-top: 15px;
        padding-bottom: 1.5rem;
        transition: var(--transition-normal);
        p {
            margin: 15px 0;
        }
        a {
            text-decoration: none;
            color: var(--colour-blue-00);
            width: 100%;
            display: inline-block;
        }
        .read--more-btn {
            font-size: 16px;
            line-height: 16px;
            font-family: var(--font-bold);
            text-transform: uppercase;
            letter-spacing: 0.05em;
        }
    }
`;