import React, { useEffect } from 'react';
import { connect, styled } from 'frontity';
import dayjs from "dayjs";
import Link from "../ui/link";

const PostsList = ({ state, actions, items, single, featured, libraries }) => {
    const Html2React = libraries.html2react.Component;
    // const data = state.source.get('/our-insights/');
    // // const { items } = data;

    // useEffect(() => {
    //     actions.source.fetch('/our-insights/');
    // }, []);

    return (
        <div className="row">
            {items.map((item, i) => {
                const { post_type, type, ID, id } = item;
                const postData = state.source[post_type ? post_type : type][ID ? ID : id];
                const { categories, featured_media, link } = postData;
                const title = postData.title.rendered;
                const excerpt = postData.excerpt.rendered;
                const fmedia = state.source.attachment[featured_media];
                let source_url = null;
                if (fmedia !== undefined && fmedia !== null) {
                    source_url = fmedia.source_url;
                }
                const formattedDate = dayjs(postData.date).format("DD MMMM YYYY");
                const category = state.source['category'][categories[0]];
                const { name } = category;
                return (
                    <PostItem key={i.toString()} className={`p-0 p-md-3 mb-5 mb-md-0 position-relative ${single ? "col" : featured ? "col-md-6" : "col-md-4"}`}>
                        <div className="pe-md-5 mb-5 mb-md-0">
                            {source_url && <img src={source_url} alt={title} />}
                        </div>
                        <div className="post--item-wrapper px-5 pe-md-0 ps-md-5">
                            <div className="post--meta px-4 py-3">
                                <b>{name} </b>{formattedDate}
                            </div>
                            <div className="post--content position-relative px-4">
                                <Link link={link}><h4>{title}</h4></Link>
                                {featured && <Html2React html={excerpt} />}
                                <Link link={link} className="d-flex align-items-center read--more-btn mt-4">
                                    <span className="me-2">Read more</span>
                                    <svg width="9" height="17" viewBox="0 0 9 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.99991 16L6.33398 8.49989L1.99991 1" stroke="#00C7EE" strokeWidth="3" strokeMiterlimit="10"/></svg>
                                </Link>
                            </div>
                        </div>
                    </PostItem>
                );
            })}
    </div>
    );
}

export default connect(PostsList);

const PostItem = styled.div`
    .post--item-wrapper {
        @media screen and (max-width: 991px) {
            position: absolute;
            z-index: 1;
            bottom: 0;
        }
        @media screen and (min-width: 992px) {
            transform: translateY(-70%);
        }
        &:hover {
            .post--meta {
                background-color: var(--colour-blue-01);
            }
            .post--content {
                padding-top: 1.5rem;
                padding-bottom: 3rem;
            }
            .read--more-btn {
                opacity: 1;
                visibility: visible;
            }
        }
    }
    .post--meta {
        background-color: var(--colour-blue-01);
        transition: var(--transition-normal);
        font-size: 16px;
        line-height: 16px;
        @media screen and (min-width: 992px) {
            background-color: var(--colour-grey-01);
        }
    }
    .post--content {
        background-color: var(--colour-white-00);
        padding-top: 30px;
        padding-bottom: 1.5rem;
        transition: var(--transition-normal);
        @media screen and (min-width: 992px) {
            padding-top: 60px;
        }
        p {
            margin: 15px 0;
        }
        a {
            text-decoration: none;
            color: var(--colour-blue-00);
            width: 100%;
            display: inline-block;
        }
        .read--more-btn {
            font-size: 16px;
            line-height: 16px;
            font-family: var(--font-bold);
            text-transform: uppercase;
            letter-spacing: 0.05em;
            @media screen and (min-width: 992px) {
                position: absolute;
                left: 1.5rem;
                bottom: 1.5rem;
                transition: var(--transition-normal);
                opacity: 0;
                visibility: hidden;
            }
        }
    }
`;