import React, { useEffect } from 'react';
import { connect, styled } from 'frontity';
import Hero from '../hero';
import FlexibleContent from '../flexible';

const Homepage = ({ state, actions, data }) => {
    const post = state.source[data.type][data.id];
    const flexible_content = post.acf.flexible_content;
    const fmedia = state.source.attachment[post.featured_media];
    let source_url = null;
    if (fmedia !== undefined) {
        source_url = fmedia.source_url;
    }
    
    useEffect(() => {
        actions.source.fetch(state.router.link);
    }, []);

    const page = state.source.get(state.router.link);

    return page.isReady ? (
        <>
            <Hero 
                post={post} 
                data={data}
                image={source_url}
                className="hero--homepage"
                icon={data.isServices ? true : false}
                large={data.isSolutions || data.isIndustries ? false : true}
            />

            {flexible_content && <FlexibleContent flexibleContent={flexible_content} />}
        </>
    ) : null;
}

export default connect(Homepage);