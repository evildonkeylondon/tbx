import React, { useEffect } from 'react';
import { connect, styled } from 'frontity';
import Link from '../ui/link';

const Default = ({ state, actions, data, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const post = state.source[data.type][data.id];
    
    useEffect(() => {
        actions.source.fetch(state.router.link);
    }, []);

    const page = state.source.get(state.router.link);

    return page.isReady ? (
        <>
            <div className="container">
                <div className="row">
                    <div className="col">
                        Page
                    </div>
                </div>
            </div>
        </>
    ) : null;
}

export default connect(Default);