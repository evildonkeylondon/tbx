import React, { useEffect } from 'react';
import { connect } from 'frontity';
import FlexibleContent from '../flexible';
import Hero from '../hero';
import Column from '../column';
import Cta from '../cta';

const PostType = ({ state, actions }) => {

    const data = state.source.get(state.router.link);
    const post = state.source[data.type][data.id];
    const { blue_0 } = state.theme.colours;
    const intro_copy = data.isSolutions || data.isServices || data.isIndustries ? post.acf.intro_copy : null;
    const side_copy = data.isSolutions || data.isServices || data.isIndustries ? post.acf.side_copy : null;
    const inner_side_copy = data.isSolutions || data.isServices || data.isIndustries ? post.acf.inner_side_copy : null;
    const content = post.content.rendered;
    const cta = post.acf.cta;
    const flexible_content = post.acf.flexible_content;

    const fmedia = state.source.attachment[post.featured_media];
    let source_url = null;
    if (fmedia !== undefined) {
        source_url = fmedia.source_url;
    }

    useEffect(() => {
        actions.source.fetch(state.router.link);
    }, []);

    return data.isReady ? (
        <>
            <Hero 
                bg={blue_0} 
                post={post} 
                data={data}
                icon={data.isServices ? true : false}
                large={data.isServices ? true : false}
            />

            {intro_copy && <div className="container my-5 py-md-3">
                <div className={`row ${side_copy ? `justify-content-between` : `justify-content-center`}`}>
                    <div className={`col-md-${side_copy ? `8` : `9`}`}>
                        <Column content={intro_copy} />
                    </div>
                    {side_copy && <div className="col-md-3">
                        <Column content={side_copy} sideText={true} />
                    </div>}
                </div>    
            </div>}

            {source_url && <img src={source_url} alt={post.title.rendered} />}

            {content && <div className="container my-5 py-md-3">
                <div className={`row ${inner_side_copy ? `justify-content-between` : `justify-content-center`}`}>
                    <div className={`col-md-${inner_side_copy ? `8` : `9`}`}>
                        <Column content={content} />
                        {cta && <Cta cta={cta} />}
                    </div>
                    {inner_side_copy && <div className="col-md-3">
                        <Column content={inner_side_copy} sideText={true} />
                    </div>}
                </div>    
            </div>}

            {flexible_content && <FlexibleContent flexibleContent={flexible_content} />}

        </>
    ) : null;
};

export default connect(PostType);