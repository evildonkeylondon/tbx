import React, { useEffect, useRef } from 'react';
import { connect, styled } from 'frontity';
// import mixitup from 'mixitup';
import Hero from '../hero';
import PostsList from "../posts";
import Grid from "../posts/grid";
import Dropdown from "../dropdown";

const News = ({ state, actions, data, libraries }) => {
    const gridMix = useRef();

    actions.theme.dropdownChange('*');

    const Html2React = libraries.html2react.Component;
    const post = state.source[data.type][data.id];
    const blog = state.source.post;
    const items = Object.values(blog);
    const featuredItems = items.slice(0, 2);
    const gridItems = items.slice(2);
    // console.log(items);

    useEffect(() => {
        actions.source.fetch(state.router.link);
        actions.source.fetch('/industries');
        actions.source.fetch('/solutions');
        actions.source.fetch('/services');
    }, []);

    const page = state.source.get(state.router.link);
    const industries = state.source.get('/industries');
    const solutions = state.source.get('/solutions');
    const services = state.source.get('/services');

    return page.isReady && industries.isReady && solutions.isReady && services.isReady ? (
        <>
            <PostWrapper>
                <Hero 
                    post={post} 
                    data={data}
                    icon={false}
                    large={false}
                />

                {/* <div className="container mt-5 pt-5">
                    <div className="row">
                        <PostsList items={featuredItems} featured={true} />
                    </div>
                </div> */}
            </PostWrapper>

            <div className="container my-5 pt-5">
                <div className="row mb-5">
                    <div className="col d-flex flex-column flex-md-row align-items-center">
                        <h5 className="me-4 mb-4 mb-md-0">Filter by</h5>
                        <Dropdown items={industries.items} title="industry" />
                        <Dropdown items={solutions.items} title="solution" />
                        <Dropdown items={services.items} title="service" />
                    </div>
                </div>
                <div className="row">
                    <div className="col" ref={gridMix}>

                    </div>
                </div>
                <Grid items={items} />
            </div>
        </>
    ) : null;
}

export default connect(News);

const PostWrapper = styled.div`
    background: linear-gradient(180deg, #FFFFFF 0.01%, #F3F4F4 100%);
    display: inline-block;
    padding-top: 90px;
    width: 100%;
`;