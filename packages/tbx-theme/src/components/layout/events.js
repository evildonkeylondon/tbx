import React, { useEffect } from 'react';
import { connect, styled } from 'frontity';
import Hero from '../hero';
import Grid from "../posts/grid";

const Events = ({ state, actions, data, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const post = state.source[data.type][data.id];
    
    const page = state.source.get('/events');
    const { items } = page;

    useEffect(() => {
        actions.source.fetch('/events');
    }, []);

    return page.isReady ? (
        <>
            <PostWrapper>
                <Hero 
                    post={post} 
                    data={data}
                    icon={false}
                    large={false}
                />

                <div className="container mt-5 pt-5">
                    <Grid items={items} />
                </div>
            </PostWrapper>
        </>
    ) : null;
}

export default connect(Events);

const PostWrapper = styled.div`
    display: inline-block;
    padding-top: 90px;
    width: 100%;
`;