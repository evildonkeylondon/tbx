import React, { useEffect } from 'react';
import { connect, styled } from 'frontity';
import Hero from '../hero';
import PostsList from "../posts";

const Post = ({ state, actions, data, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const post = state.source[data.type][data.id];
    const content = post.content.rendered;
    const tags = post.tags;
    const { related_insights, authors, related_sidebar, services, solutions, industries } = post.acf;
    
    useEffect(() => {
        actions.source.fetch(state.router.link);
    }, []);

    const page = state.source.get(state.router.link);

    return page.isReady ? (
        <PostWrapper>
            
            <Hero 
                post={post} 
                data={data}
                icon={false}
                large={false}
            />

            <div className="container mb-5">
                <div className="row mb-5">
                    <div className="col-md-2 mb-5">
                        {authors && <h5>Authors</h5>}
                        {authors && authors.map((author, i) => {
                            return (
                                <div key={i.toString()}>
                                    <p><b>{author.name}</b><br/>{author.job_title}</p>
                                </div>
                            )
                        })}

                        <div className="my-5">
                            {tags && <h5>Authors</h5>}
                            {tags && <TagsList className="m-0 p-0 list-unstyled d-flex flex-wrap">
                                {tags.map((tag, i) => {
                                    return (
                                        <li key={i.toString()}>{state.source.tag[tag].name}</li>
                                    )
                                })}
                                </TagsList>
                            }
                        </div>
                    </div>
                    {content && <div className="col-md-7 mb-5">
                        <Html2React html={content} />
                    </div>}

                    {related_sidebar && <div className="col-md-3 mb-5">
                        <div className="row mb-4">
                            <div className="col">
                                <h5>Related insight</h5>
                            </div>
                        </div>
                        <PostsList items={related_sidebar} single={true} />
                    </div>}
                </div>
                {related_insights && <>
                    <div className="row pt-5 mb-4">
                        <div className="col">
                            <h5>Related insights</h5>
                        </div>
                    </div>
                    <PostsList items={related_insights} />
                </>}
            </div>
        </PostWrapper>
    ) : null;
}

export default connect(Post);

const PostWrapper = styled.div`
    background: linear-gradient(180deg, #FFFFFF 0.01%, #F3F4F4 100%);
    display: inline-block;
    width: 100%;
`;

const TagsList = styled.ul`
    li {
        font-size: 18px;
        line-height: 16px;
        padding: 12px;
        margin-right: 5px;
        margin-bottom: 5px;
        border: 2px solid var(--colour-grey-01);
    }
`;