import React, { useEffect } from 'react';
import { connect } from 'frontity';
import Hero from '../hero';
import Column from '../column';
import FlexibleContent from '../flexible';

const FlexiblePage = ({ state, actions, data }) => {

    const post = state.source[data.type][data.id];
    const content = post.content.rendered;
    const { flexible_content } = post.acf;
    const fmedia = state.source.attachment[post.featured_media];
    let source_url = null;
    if (fmedia !== undefined) {
        source_url = fmedia.source_url;
    }
    
    useEffect(() => {
        actions.source.fetch(state.router.link);
    }, []);

    const page = state.source.get(state.router.link);

    return page.isReady ? (
        <>
            <Hero 
                post={post} 
                data={data}
                image={source_url}
                icon={data.isServices ? true : false}
                large={true}
            />

            {content && <div className="container mb-5 py-md-3">
                <div className="row justify-content-center">
                    <div className="col-md-9">
                        <Column content={content} />
                    </div>
                </div>    
            </div>}

            {flexible_content && <FlexibleContent flexibleContent={flexible_content} />}
        </>
    ) : null;
}

export default connect(FlexiblePage);