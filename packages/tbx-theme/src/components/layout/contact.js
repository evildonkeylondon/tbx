import React, { useEffect } from 'react';
import { connect, styled } from 'frontity';
import Hero from '../hero';
import Column from '../column';
import FlexibleContent from '../flexible';

const Contact = ({ state, actions, data, libraries }) => {
    const Html2React = libraries.html2react.Component;
    const post = state.source[data.type][data.id];
    const content = post.content.rendered;
    const { flexible_content, details } = post.acf;
    
    useEffect(() => {
        actions.source.fetch(state.router.link);
    }, []);

    const page = state.source.get(state.router.link);

    return page.isReady ? (
        <ContactWrapper>

            <Hero 
                post={post} 
                data={data}
                icon={false}
                large={false}
            />

            {content && <div className="container mb-5 py-md-3">
                <div className="row">
                    <div className="col mb-4">
                        <h5>Get in touch</h5>
                    </div>
                </div>
                <div className="row justify-content-between">
                    <ContentWrapper className="col-md-8">
                        <Column content={content} />
                    </ContentWrapper>
                    {details && <DetailsWrapper className="col-md-3">
                        <Column content={details} />    
                    </DetailsWrapper>}
                </div>    
            </div>}

            {flexible_content && <FlexibleContent flexibleContent={flexible_content} />}
        </ContactWrapper>
    ) : null;
}

export default connect(Contact);

const ContactWrapper = styled.div`
    background: linear-gradient(180deg, #FFFFFF 0.01%, #F3F4F4 100%);
    display: inline-block;
    width: 100%;
`;

const ContentWrapper = styled.div`
    border-top: 1px solid var(--colour-grey-01);
`;

const DetailsWrapper = styled.div`
    border-top: 1px solid var(--colour-grey-01);
    padding-top: 30px;
`;