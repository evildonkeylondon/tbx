import React, { useRef, useEffect } from 'react';
import { connect, styled } from "frontity";
import Link from '../ui/link';
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

const Banner = ({ state }) => {
    let arrow1Ref = useRef();
    let arrow2Ref = useRef();
    let arrow3Ref = useRef();

    useEffect(() => {

        gsap.set(arrow1Ref.current, {x: -20});
        gsap.set(arrow2Ref.current, {x: -40});
        gsap.set(arrow3Ref.current, {x: -80});

        gsap.to(arrow1Ref.current, {
            duration: .5,
            x: 0,
            scrollTrigger: {
                id: "arrow1-ref",
                trigger: arrow1Ref.current,
                start: "top 70%",
                toggleActions: "play none none reverse",
                scrub: false
            }
        });

        gsap.to(arrow2Ref.current, {
            duration: .5,
            x: 0,
            scrollTrigger: {
                id: "arrow2-ref",
                trigger: arrow2Ref.current,
                start: "top 70%",
                toggleActions: "play none none reverse",
                scrub: false
            }
        });

        gsap.to(arrow3Ref.current, {
            duration: .5,
            x: 0,
            scrollTrigger: {
                id: "arrow3-ref",
                trigger: arrow3Ref.current,
                start: "top 70%",
                toggleActions: "play none none reverse",
                scrub: false
            }
        });
        
        return () => {
            const arrow1Animation = ScrollTrigger.getById(`arrow1-ref`);
            const arrow2Animation = ScrollTrigger.getById(`arrow2-ref`);
            const arrow3Animation = ScrollTrigger.getById(`arrow3-ref`);
			if (arrow1Animation) {
				arrow1Animation.kill(true);
			}
            if (arrow2Animation) {
				arrow2Animation.kill(true);
			}
            if (arrow3Animation) {
				arrow3Animation.kill(true);
			}
        }

    }, []);

    return (
        <BannerWrapper>
            <svg width="550" height="150" viewBox="0 0 550 150" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g ref={arrow1Ref}>
                    <path opacity="0.8" d="M302 -161L438.289 75.3992H549.761L413.458 -161H302Z" fill="white"/>
                    <path d="M438.289 75.3992L302 311.805H413.458L549.761 75.3992H438.289Z" fill="white"/>
                </g>
                <g ref={arrow2Ref}>
                    <path d="M151 -161L287.289 75.3992H398.761L262.458 -161H151Z" fill="#243746"/>
                    <path d="M287.289 75.3992L151 311.805H262.458L398.761 75.3992H287.289Z" fill="#051C2C"/>
                </g>
                <g ref={arrow3Ref}>
                    <path d="M0 -161L136.289 75.3992H247.761L111.458 -161H0Z" fill="#243746"/>
                    <path d="M136.289 75.3992L0 311.805H111.458L247.761 75.3992H136.289Z" fill="#051C2C"/>
                </g>
            </svg>

            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-10">
                        <Link 
                            link="/contact-us"
                            children={<>Ready to work with us? <span>Get in touch</span></>}
                        />
                    </div>
                </div>
            </div>
        </BannerWrapper>
    );
}

export default connect(Banner);

const BannerWrapper = styled.div`
    width: 100%;
    display: inline-block;
    background-color: var(--colour-blue-01);
    padding: 50px 0;
    overflow: hidden;
    position: relative;
    svg {
        position: absolute;
        right: 50px;
        top: 50%;
        transform: translateY(-50%);
    }
    a {
        color: var(--colour-blue-00);
        font-size: 36px;
        line-height: 36px;
        text-decoration: none;
        font-family: var(--font-regular);
        span {
            font-family: var(--font-light);
        }
    }
`;