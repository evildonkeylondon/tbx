import React from 'react';
import { connect, styled } from 'frontity';
import Icons from '../icons';
import Cta from '../cta';
import dayjs from "dayjs";

const Hero = ({ state, libraries, data, post, bg, image, large, className, icon }) => {

    const Html2React = libraries.html2react.Component;
    const title = post.title.rendered;
    const { heading, cta, reading_time } = post.acf;
    const content = post.content.rendered;
    const formattedDate = dayjs(post.date).format("DD MMMM YYYY");
    const category = data.isPost ? state.source.category[post.categories[0]].name : null;

    return (
        <HeroWrapper bg={bg} image={image} className={`d-flex ${className ? className : ''} ${(data.link === "/contact-us/" || data.isPost) ? "my-5" : "mb-md-5"}`} large={large}>
            <div className="container d-flex flex-grow-1">
                <div className={`row w-100 flex-grow-1 align-items-center ${data.isHome ? 'justify-content-center' : ''}`}>
                    <div className={`${(data.link === "/contact-us/" || data.link === "/our-insights/") ? "col-md-8 offset-md-1" : (data.link === "/our-events/") ? "col-md-7 offset-md-1" : "col-md-6"} pt-5 my-5`}>
                        {data.isSolutions && <h3>Our solutions</h3>}
                        {data.isServices && <h3>Our services</h3>}
                        {data.isIndustries && <h3>Verticles</h3>}
                        {(data.link === "/our-approach/" || data.link === "/about-us/" || data.link === "/partners/") && <h3>{title}</h3>}
                        {(data.link === "/contact-us/" || data.link === "/our-insights/" || data.link === "/our-events/") && <h3 className="contact--heading">{title}</h3>}
                        {data.isPost && <h3 className="contact--heading">{category}</h3>}
                        <h1 className={`pe-5 pe-md-0 ${data.isHome && "pt-5 pt-md-0"}`}><Html2React html={heading ? heading : title} /></h1>
                        {data.isPost && <div className="post--metadata pt-4">
                            <span>{formattedDate}</span><span className="reading--time ms-4">{reading_time}</span>
                        </div>}
                    </div>
                    {icon && data.id == 101 && <div className="col-md-6 pt-5 my-5">{Icons[0]}</div>}
                    {icon && data.id == 102 && <div className="col-md-6 pt-5 my-5">{Icons[1]}</div>}
                    {icon && data.id == 99 && <div className="col-md-6 pt-5 my-5">{Icons[2]}</div>}
                    {icon && data.id == 103 && <div className="col-md-6 pt-5 my-5">{Icons[3]}</div>}
                    {icon && data.id == 105 && <div className="col-md-6 pt-5 my-5">{Icons[4]}</div>}
                    {icon && data.id == 107 && <div className="col-md-6 pt-5 my-5">{Icons[5]}</div>}
                    {icon && data.id == 104 && <div className="col-md-6 pt-5 my-5">{Icons[6]}</div>}
                    {data.isHome && <div className="col-md-6 align-self-end p-0 p-md-3">
                        <HomeBox className="p-5 p-md-4">
                            <Html2React html={content} />
                            {cta && <Cta cta={cta} className="pt-4" />}
                        </HomeBox>
                    </div>}
                </div>
            </div>
        </HeroWrapper>
    );
};

export default connect(Hero);

const HeroWrapper = styled.div`
    background-color: ${props => props.bg ? props.bg : null};
    background-image: url(${props => props.image});
    background-size: cover;
    background-position: center center;
    color: ${props => props.bg || props.image ? "var(--colour-white-00)" : "var(--colour-blue-00)"};
    min-height: ${props => props.large ? "100vh" : null};
    position: relative;
    h3 {
        color: var(--colour-blue-01);
        font-family: var(--font-bold);
        &.contact--heading {
            color: var(--colour-blue-02);
        }
    }
    .post--metadata {
        font-size: 16px;
        line-height: 16px;
        .reading--time {
            display: inline-block;
            font-family: var(--font-bold);
        }
    }
`;

const HomeBox = styled.div`
    background-color: var(--colour-blue-00);
    font-size: 18px;
    line-height: 26px;
    @media screen and (min-width: 992px) {
        transform: translateY(24px);
    }
`;