import Root from "./components";
import acfOptionsHandler from './components/handlers/acfOptionsHandler';
import menuHandler from './components/handlers/menuHandler';
import image from "@frontity/html2react/processors/image";
import link from "@frontity/html2react/processors/link";

export default {
  name: "tbx-theme",
  roots: {
    theme: Root
  },
  state: {
    theme: {
      mainMenu: "main-menu",
      footerMenu: "footer-menu",
      colours: {
        white: "#ffffff",
        black: "#000000",
        blue_0: "#051C2C",
        blue_1: "#66DDF5"
      },
      windowWidth: 0,
      isModalOpen: false,
      isMobileMenuOpen: false,
      isOverlayVisible: true,
      selectedFilterId: false,
      selectedFilterType: null,
      storedGridItems: {
        filtered: false,
        industry: false,
        service: false,
        solution: false
      }
    }
  },
  actions: {
    theme: {
      autoPrefetch: "hover",
      beforeSSR: async ({ state, actions }) => {
        await Promise.all([
          await actions.source.fetch(`/`),
          await actions.source.fetch("theme-tbx-settings"),
          await actions.source.fetch(`/menu/${state.theme.mainMenu}/`),
          await actions.source.fetch(`/menu/${state.theme.footerMenu}/`)
        ]);
      },
      modalOpen: ({ state }) => {
        state.theme.isModalOpen = !state.theme.isModalOpen;
      },
      toggleMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = !state.theme.isMobileMenuOpen;
      },
      closeMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = false;
      },
      dropdownChange: ({ state }) => value => {
        state.theme.selectedFilterId = value;
      },
      dropdownChangeType: ({ state }) => value => {
        state.theme.selectedFilterType = value;
      },
      updateStoredGridItems: ({ state }) => (value, type, filtered) => {
        state.theme.storedGridItems = {
          ...state.theme.storedGridItems,
          filtered: filtered,
          [type]: value
        };
      }
    }
  },
  libraries: {
    html2react: {
      processors: [image, link]
    },
    source: {
      handlers: [acfOptionsHandler, menuHandler]
    }
  }
};
