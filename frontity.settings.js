const settings = {
  "name": "tbx",
  "state": {
    "frontity": {
      "url": "https://test.frontity.org",
      "title": "Test Frontity Blog",
      "description": "WordPress installation for Frontity development"
    }
  },
  "packages": [
    {
      "name": "tbx-theme",
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "url": "https://davidc398.sg-host.com",
          "homepage": "/homepage",
          // "postsPage": "/our-insights",
          "postTypes": [
            {
              "type": "solutions",
              "endpoint": "solutions",
              "archive": "/solutions"
            },
            {
              "type": "services",
              "endpoint": "services",
              "archive": "/services"
            },
            {
              "type": "industries",
              "endpoint": "industries",
              "archive": "/industries"
            },
            {
              "type": "events",
              "endpoint": "events",
              "archive": "/events"
            },
          ],
          "taxonomies": [
            {
              "taxonomy": "events_categories",
              "endpoint": "events_categories",
              "postTypeEndpoint": "events",
            }
          ]
        }
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react",
    "@frontity/head-tags",
    "@aamodtgroup/frontity-contact-form-7",
    "@frontity/yoast",
    {
      "name": "@frontity/google-analytics",
      "state": {
        "googleAnalytics": {
          "trackingId": "###",
        },
      },
    }
  ]
};

export default settings;

// http://tbx.local
// https://davidc398.sg-host.com